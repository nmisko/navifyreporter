require 'rubygems'
require 'httparty'
require 'nokogiri'   
require 'byebug'

userName = ARGV.first
date = ARGV[1]

#puts "User name is #{userName}"
#puts "Date of execute is #{date}"

file = File.read('./simple/report.html')
doc = Nokogiri::HTML(file)
#		<li id="summaryTotalTestCases">Total Test Cases: </li>
node = doc.css('#summary > ul:nth-child(2)')
who_node = Nokogiri::XML::Node.new "li", doc
who_node.content = "Executed by: #{userName}"
node[0].add_child(who_node)


when_node = Nokogiri::XML::Node.new "li", doc
when_node.content = "Date: #{userName}"
node[0].add_child(when_node)


File.write('./simple/report.html', doc.to_xml)
